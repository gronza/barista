using System;
using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        public GameObject manager;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;

            target = GameObject.Find("TargetPoint").transform;

            manager = GameObject.Find("GameManager");
        }
        
        private void Update()
        {
            MoveToTarget();
        }

        private void MoveToTarget()
        {
            if (target != null)
            {
                agent.SetDestination(target.position);

            }
            if (agent.remainingDistance > agent.stoppingDistance)
            {
                character.Move(agent.desiredVelocity, false, false);
            }
            else
            {
                character.Move(Vector3.zero, false, false);

            }
        }

        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void BuyDrink()
        {
            //the customers stops at the bar to buy a drink before heading to a table
            IEnumerator coroutine = WaitAndDrink(2);
            StartCoroutine(coroutine);
        }

        private IEnumerator WaitAndDrink(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            Debug.Log("Drink bought");

            //checks that the target chair has no customer sitting on it already
            if (!target.CompareTag("Chair"))
            {
                do
                {
                    var amountOfChairs = GameObject.FindGameObjectsWithTag("Chair").Length;
                    var targetChair = UnityEngine.Random.Range(0, amountOfChairs);

                    if (!GameObject.FindGameObjectsWithTag("Chair")[targetChair].GetComponent<ChairLogic>().reserved)
                    {
                        SetTarget(GameObject.FindGameObjectsWithTag("Chair")[targetChair].transform);
                        //reserves the chair for the customer
                        target.GetComponent<ChairLogic>().reserved = true;
                    }

                } while (target.GetComponent<ChairLogic>().reserved == false);
            }
            
            manager.GetComponent<GameManager>().AddMoney(5);
        }

        public void HeadHome()
        {
            //the customers heads back to the door and is destroyed upon impact
            IEnumerator coroutine = WaitAndGoHome(UnityEngine.Random.Range(5, 10));
            StartCoroutine(coroutine);
        }

        private IEnumerator WaitAndGoHome(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);

            //frees the reservation of the chair upon leaving
            if(target.CompareTag("Chair"))
            target.GetComponent<ChairLogic>().reserved = false;

            SetTarget(GameObject.Find("Doormat").transform);
        }

        private void OnTriggerEnter(Collider other)
        {
            //Customer proceeds to buy a drink when they reach the bar
            if (other.gameObject.CompareTag("BarPoint") && target.CompareTag("BarPoint"))
            {
                BuyDrink();
            }
            //Customer drinks for an amount of time and heads to the door after
            else if (other.gameObject.CompareTag("Chair") && other.transform == target.transform)
            {
                HeadHome();
            }
            //Customer is destroyed when they reach the door
            else if (other.gameObject.CompareTag("DoorMat") && target.CompareTag("DoorMat"))
            {
                    Destroy(gameObject);
                    manager.GetComponent<CustomerSpawner>().customerCount -= 1;
                    
            }
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Customer"))
            {
                Physics.IgnoreCollision(collision.collider, GetComponent<CapsuleCollider>(), true);
            }
        }
    }
}
