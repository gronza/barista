﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerSpawner : MonoBehaviour {

    public float spawnTimer;
    public int maxCustomers;
    public int customerCount;

    public GameObject customer;
    public Transform doorMat;

    public float varyingSpawnTimer;

	// Use this for initialization
	void Start () {
        varyingSpawnTimer = Random.Range(6, 8);
        spawnTimer = varyingSpawnTimer;

        maxCustomers = GameObject.FindGameObjectsWithTag("Chair").Length;
        customerCount = 1;
	}
	
	// Update is called once per frame
	void Update () {

        spawnTimer -= Time.deltaTime;

        //Spawns a customer
        if(spawnTimer <= 0 && customerCount < maxCustomers)
        {
            Instantiate(customer,
                doorMat.position,
                new Quaternion());

            customerCount++;
            spawnTimer = varyingSpawnTimer;
        }

        //Sets the customer count visual
        GameObject.Find("ChairsAmount").GetComponent<Text>().text
            = "Customers: " + customerCount+"/"+maxCustomers;
	}
}
