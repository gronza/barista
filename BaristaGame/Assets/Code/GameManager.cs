﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public int money;
    public Text moneyText;

	// Use this for initialization
	void Start () {
        moneyText = GameObject.Find("Money").GetComponent<Text>();

        money = 100;
	}
	
	// Update is called once per frame
	void Update () {

        //Updating the money UI element
        moneyText.text = money.ToString() + " €";

        if(money < 0)
        {
            Time.timeScale = 0;
        }
	}

    public void AddMoney(int x)
    {
        money = money + x;
    }
}
