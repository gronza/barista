﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLogic : MonoBehaviour {

    public float dayTimer;
    public string day;
    public float dayDuration;

    public int rent;

	// Use this for initialization
	void Start () {
        dayTimer = 0.1f;
        dayDuration = 5;

        rent = 50;
	}
	
	// Update is called once per frame
	void Update () {
        dayTimer += Time.deltaTime;
        setDay();
        GameObject.Find("Day").GetComponent<Text>().text = day;
	}

    public void setDay()
    {
        if(dayTimer < dayDuration)
        {
            day = "Monday";
        }if(dayTimer > dayDuration)
        {
            day = "Tuesday";
        }
        if (dayTimer > dayDuration*2)
        {
            day = "Wednesday";
        }
        if (dayTimer > dayDuration*3)
        {
            day = "Thursday";
        }
        if (dayTimer > dayDuration*4)
        {
            day = "Friday";
        }
        if (dayTimer > dayDuration*5)
        {
            day = "Saturday";
        }
        if (dayTimer > dayDuration*6)
        {
            day = "Sunday";
        }
        if (dayTimer > dayDuration*7)
        {
            dayTimer = 0;
            GameObject.Find("GameManager").GetComponent<GameManager>().AddMoney(-rent);
        }
    }
}
